﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Reflection.MethodBase;

namespace ocrupdate_batch_upload
{
    //保険者名を貰って、バッチファイルを作成
    class Program
    {
        #region 更新用クラス定義
        /// <summary>
        /// application更新用クラス
        /// </summary>
        public partial class AppforAWSUpdate
        {
            public int aid;
            public string ocrdata;
            public int statusflags;
        }

        /// <summary>
        /// scan更新用クラス
        /// </summary>
        public partial class ScanforAWSUpdate
        {
            public int scanid;
            public int status;
        }

        /// <summary>
        /// scangroup更新用クラス
        /// </summary>
        public partial class ScanGroupforAWSUpdate
        {
            public int groupid;
            public int status;
        }
        #endregion

        public static string strDBHost = string.Empty;
        public static int intDBPort = 0;
        public static string strDBUser = string.Empty;
        public static string strDBPass = string.Empty;
        public static string strDBName = string.Empty;
        public static int intTimeOut = 0;

        #region バッチ作成用変数
        public enum SCAN_STATUS { NULL = 0, SCANING = 1, 取込済み = 2, OCR済み = 3, UPload済み削除対象 = 4, }//アップロード完了フラグ4追加。アップロード完了時にこれに更新


        public enum APP_StatusFlag
        {
            未処理 = 0x0,
            入力済 = 0x1, ベリファイ済 = 0x2, 自動マッチ済 = 0x4, 入力時エラー = 0x8,
            点検対象 = 0x10, 点検済 = 0x20, 返戻 = 0x40, 支払保留 = 0x80,
            照会対象 = 0x100, 保留 = 0x200, 過誤 = 0x400, 再審査 = 0x800,
            往療点検対象 = 0x1000, 往療疑義 = 0x2000, /*往療疑義なし = 0x4000,*/ 往療点検済 = 0x8000,
            追加入力済 = 0x1_0000, 追加ベリ済 = 0x2_0000, 拡張入力済 = 0x4_0000, 拡張ベリ済 = 0x8_0000,
            架電対象 = 0x10_0000, 支払済 = 0x20_0000, 処理1 = 0x40_0000, 処理2 = 0x80_0000,
            処理3 = 0x100_0000, 処理4 = 0x200_0000, 処理5 = 0x400_0000,
            独自処理1 = 0x1000_0000, 独自処理2 = 0x2000_0000,
        }

        public enum GroupStatus
        {
            OCR待ち = 0, OCR中 = 1, OCR済み = 2, 入力中 = 3, 入力済み = 4,
            ベリファイ済み = 5, 点検中 = 6, 点検済み = 7,
        }


        public static string strEncode = "utf8";//sqlファイルの文字コード
                                                //string strEncode = "shift-jis";//sqlファイルの文字コード
        public static string strPsqlPath = string.Empty;//"C:\\program files\\postgresql\\11\\bin";//psql.exeのあるフォルダパス


        #endregion

        public static string strOutputPath = string.Empty;//バッチ、sqlファイルの作成場所

        #region 更新用クラスのリスト
        /// <summary>
        /// awsのscan更新用クラスのリスト
        /// </summary>
        public static List<ScanforAWSUpdate> lstScanid = new List<ScanforAWSUpdate>();

        /// <summary>
        /// awsのscangroup更新用クラスのリスト
        /// </summary>
        public static List<ScanGroupforAWSUpdate> lstScanGroup = new List<ScanGroupforAWSUpdate>();

        /// <summary>
        /// awsのapplication更新用クラスのリスト
        /// </summary>
        public static List<AppforAWSUpdate> lstApp = new List<AppforAWSUpdate>();
        #endregion

        public static System.Data.DataSet dsSetting = new System.Data.DataSet();


        public static string strErrpath = string.Empty;
        public static string strBatchSaveFolderPerLoop = string.Empty;

        /// <summary>
        /// コマンドラインパラメータ
        /// </summary>
        static string[] args;

        static void Main(string[] _args)
        {
            args = _args;


#if DEBUG
            System.Windows.Forms.MessageBox.Show("start");
#endif

            //受け取ったパラメータを変数にセット
            strDBHost = args[0];
            strDBName = args[1];
            intDBPort = int.Parse(args[2]);
            strDBUser = args[3];
            strDBPass = args[4];
            strErrpath = args[5];
            strBatchSaveFolderPerLoop = args[6];
                 
            if (!LoadSetting()) return;

            //フォルダなかったら作成
            if (!System.IO.Directory.Exists(strOutputPath)) System.IO.Directory.CreateDirectory(strOutputPath);


            if (!UpdateAWS()) return;

            UpdateScanStatusFinish();
        }

        public static void ErrMessage(string strDisp)
        {
            System.IO.StreamWriter sw = new System.IO.StreamWriter($"{strErrpath}\\errlog.txt", true) ;
            sw.WriteLine($"{DateTime.Now.ToString("yyyyMMdd_HHmmss")}:{strDisp}");
            sw.Close();

            //System.Windows.Forms.MessageBox.Show(strDisp);
        }



        /// <summary>
        /// settings.xmlロード
        /// </summary>
        /// <returns></returns>
        private static bool LoadSetting()
        {

            try
            {
                dsSetting.ReadXml("settings.xml");
                strOutputPath = $"{dsSetting.Tables[0].Rows[0]["UploadBatchOutputPath"].ToString()}\\{strBatchSaveFolderPerLoop}";
                strPsqlPath = $"{dsSetting.Tables[0].Rows[0]["psqlPath"].ToString()}";

                return true;
            }
            catch (Exception ex)
            {
                ErrMessage(GetCurrentMethod().Name + ex.Message);
                return false;
            }

        }

        /// <summary>
        /// 保険者のDBに接続
        /// </summary>
        /// <returns></returns>
        private static bool UpdateAWS()
        {

            try
            {

                if (!getListFromLocalOCRDB()) return false;

                CreateAndRunBatch();

                return true;
            }

            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(strDBName + ex.Message);

                return false;
            }

        }



        /// <summary>
        /// バッチ作成+実行
        /// </summary>
        private static void CreateAndRunBatch()
        {
            CreateBatchFile_scan();
            CreateBatchFile_App();
            CreateBatchFile_scangroup();

        }

        #region 更新データ用リスト取得
        /// <summary>
        /// ローカルScanテーブルのScanIDを取得し、更新対象のApplication.AID、ScanGroup.groupidのリストを作成
        /// </summary>
        /// <returns></returns>
        public static bool getListFromLocalOCRDB()
        {
            Npgsql.NpgsqlConnection cn = new Npgsql.NpgsqlConnection();
            Npgsql.NpgsqlCommand cmd = new Npgsql.NpgsqlCommand();
            Npgsql.NpgsqlDataAdapter da = new Npgsql.NpgsqlDataAdapter();

            cn.ConnectionString =
                        $"Server={dsSetting.Tables["localocrdb"].Rows[0]["dbserver"].ToString()};" +
                        $"port={dsSetting.Tables["localocrdb"].Rows[0]["dbport"].ToString()};" +
                        $"database={strDBName};" +
                        $"user id={dsSetting.Tables["localocrdb"].Rows[0]["dbuser"].ToString()};" +
                        $"password={dsSetting.Tables["localocrdb"].Rows[0]["dbpass"].ToString()};" +
                        $"CommandTimeout = 30; timeout = 30; connectionlifetime = 30; ";


            //OCR済みのレコードを取得 ocrが走った後のcsvは、ocrimportではなく通常のscan、application,scangroupテーブルに入る
            string strsql = $"select sid,status from scan where status={(int)SCAN_STATUS.OCR済み}";


            try
            {
                cn.Open();

                cmd.Connection = cn;
                cmd.CommandText = strsql;

                //scanリスト作成
                Npgsql.NpgsqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    if (!int.TryParse(dr[0].ToString(), out int res)) continue;
                    ScanforAWSUpdate s = new ScanforAWSUpdate();
                    s.scanid = int.Parse(dr[0].ToString());
                    s.status = int.Parse(dr[1].ToString());
                    lstScanid.Add(s);



                }
                dr.Close();


                //scanリストからappリスト作成
                foreach (ScanforAWSUpdate s in lstScanid)
                {
                    strsql = $"select aid,statusflags,ocrdata from application where scanid={s.scanid}";
                    cmd.CommandText = strsql;
                    dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {

                        AppforAWSUpdate a = new AppforAWSUpdate();
                        a.aid = int.Parse(dr[0].ToString());
                        a.statusflags = int.Parse(dr[1].ToString());
                        a.ocrdata = dr[2].ToString();
                        lstApp.Add(a);

                    }
                    dr.Close();
                }


                //scanリストからgroupリスト作成
                foreach (ScanforAWSUpdate s in lstScanid)
                {
                    strsql = $"select groupid,status from scangroup where scanid={s.scanid}";
                    cmd.CommandText = strsql;
                    dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        if (!int.TryParse(dr[0].ToString(), out int res)) continue;
                        ScanGroupforAWSUpdate g = new ScanGroupforAWSUpdate();
                        g.groupid = int.Parse(dr[0].ToString());
                        g.status = int.Parse(dr[1].ToString());
                        lstScanGroup.Add(g);
                    }
                    dr.Close();
                }



                return true;

            }
            catch (Exception ex)
            {
                ErrMessage(GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
                cmd.Dispose();
                cn.Close();
            }


        }
        #endregion

        #region Scanテーブル更新バッチ
        /// <summary>
        /// AWSのScanテーブルを更新するバッチを作成する
        /// </summary>
        private static void CreateBatchFile_scan()
        {

            if (lstScanid.Count == 0) return;

            string strBatch = string.Empty;
            string strSQLFileName = $"{strDBName}_scan_update.sql";
            string strsql_scan = string.Empty;

            //バッチに食わせるsqlファイルの中身
            foreach (ScanforAWSUpdate s in lstScanid)
            {
                strsql_scan += $"update scan set status={s.status} where sid ={s.scanid} and status={(int)SCAN_STATUS.取込済み};\r\n";
            }



            //psqlバッチの中身

            strBatch += $"@echo off\r\n";
            strBatch += $"set PGCLIENTENCODING={strEncode}\r\n";
            strBatch += $"set DBNAME={strDBName}\r\n";
            strBatch += $"set FILENAME={strSQLFileName}\r\n";
            strBatch += $"set OUTPUTPATH={strOutputPath}\r\n";
            strBatch += $"mkdir %OUTPUTPATH%\r\n";
            strBatch += $"cd /d {strPsqlPath}\r\n";


            //AWSのSCANテーブルを更新する
            strBatch += $"echo %date%_%time%_%DBNAME%_upload_scan\r\n";
            strBatch += $"psql --quiet -h {strDBHost} -p {intDBPort} -U postgres -d %DBNAME% -f %OUTPUTPATH%\\{strSQLFileName}\r\n";//--command \"{strsql_scan}\";\r\n";

#if DEBUG
            strBatch += $"pause\r\n";
#endif
            System.IO.StreamWriter sw = null;
            try
            {
                //バッチに食わせるsqlファイル
                sw = new System.IO.StreamWriter(strOutputPath + $"\\{strSQLFileName}");
                sw.WriteLine(strsql_scan);
                sw.Close();

                //psqlバッチ
                sw = new System.IO.StreamWriter(strOutputPath + $"\\{strDBName}_scan_upload_{DateTime.Now.ToString("yyyyMMddHHmmss")}.bat");
                sw.WriteLine(strBatch);
                sw.Close();


                //走らせる
                RunBatch(strOutputPath + $"\\{strDBName}_scan_upload_{DateTime.Now.ToString("yyyyMMddHHmmss")}.bat");
            }
            catch (Exception ex)
            {
                ErrMessage(GetCurrentMethod().Name + ex.Message);
            }
            finally
            {

            }


        }
        #endregion

        #region app更新バッチ
        /// <summary>
        /// awsのapplicationレコードを更新するバッチファイルの作成
        /// </summary>
        private static void CreateBatchFile_App()
        {

            if (lstScanid.Count == 0) return;

            string strSQLFileName = $"{strDBName}_app_update.sql";
            string strBatch = string.Empty;
            string strsql_app = string.Empty;
            System.Text.StringBuilder sb = new StringBuilder();
            //更新するapplicationレコード。lstAppはScan.status=3(ocr済み）のレコードしかないので、全部更新する
            foreach (AppforAWSUpdate a in lstApp)
            {
                if (a.ocrdata.Trim() != "") sb.AppendLine($"update application set ocrdata='{a.ocrdata.Replace("\'", "")}' ,statusflags={a.statusflags} where aid={a.aid} and statusflags={(int)APP_StatusFlag.未処理};");
                else sb.AppendLine($"update application set statusflags={a.statusflags} where aid={a.aid} and statusflags={(int)APP_StatusFlag.未処理};");

                //if (a.ocrdata.Trim() != "") strsql_app += $"update application set ocrdata='{a.ocrdata.Replace("\'", "")}' ,statusflags={a.statusflags} where aid={a.aid};\r\n";
                //else strsql_app += $"update application set statusflags={a.statusflags} where aid={a.aid};\r\n";
            }
            
            strsql_app = sb.ToString();

            strBatch += $"@echo off\r\n";
            strBatch += $"set PGCLIENTENCODING=utf-8\r\n";//全角が入るので
            strBatch += $"set DBNAME={strDBName}\r\n";
            strBatch += $"set OUTPUTPATH={strOutputPath}\r\n";
            strBatch += $"set PSQLPATH={strPsqlPath}\r\n";

            strBatch += $"cd /d %PSQLPATH%\r\n";

            //レコードを選択しsqlファイルとして保存
            strBatch += $"echo %date%_%time%_%DBNAME%_upload_application\r\n";
            strBatch += $"psql --quiet -h {strDBHost} -p {intDBPort} -U postgres -d %DBNAME% -f %OUTPUTPATH%\\{strSQLFileName}\r\n";// --command \"{strcopycommand}\";\r\n";


#if DEBUG
            strBatch += $"pause\r\n";


#endif

            System.IO.StreamWriter sw = null;
            try
            {
                //psqlバッチに食わせるsqlファイル
                sw = new System.IO.StreamWriter(strOutputPath + $"\\{strSQLFileName}", false, System.Text.Encoding.GetEncoding("UTF-8"));
                sw.WriteLine(strsql_app);
                sw.Close();


                //psqlバッチ
                sw = new System.IO.StreamWriter(strOutputPath + $"\\{strDBName}_APP_upload_{DateTime.Now.ToString("yyyyMMddHHmmss")}.bat");
                sw.WriteLine(strBatch);
                sw.Close();

                RunBatch(strOutputPath + $"\\{strDBName}_APP_upload_{DateTime.Now.ToString("yyyyMMddHHmmss")}.bat");
            }
            catch (Exception ex)
            {
                ErrMessage(GetCurrentMethod().Name + "\r\n" + ex.Message);
            }
            finally
            {


            }

        }
        #endregion

        #region scanGroup更新バッチ
        /// <summary>
        /// awsのscangroupレコードを更新するバッチファイルの作成
        /// </summary>
        private static void CreateBatchFile_scangroup()
        {

            if (lstScanid.Count == 0) return;

            string strSQLFileName = $"{strDBName}_scangroup_update.sql";
            string strBatch = string.Empty;
            string strsql_scangroup = string.Empty;


            //lstScanGroupはScan.status=3（OCR済み）のレコードしかないので全部更新する
            foreach (ScanGroupforAWSUpdate g in lstScanGroup)
            {
                strsql_scangroup += $"update scangroup set status={g.status} where groupid={g.groupid} and status={(int)GroupStatus.OCR待ち};\r\n";
            }



            strBatch += $"@echo off\r\n";
            strBatch += $"set PGCLIENTENCODING={strEncode}\r\n";
            strBatch += $"set DBNAME={strDBName}\r\n";
            strBatch += $"set FILENAME={strDBName}_scangroup.sql\r\n";
            strBatch += $"set OUTPUTPATH={strOutputPath}\r\n";
            strBatch += $"cd /d {strPsqlPath}\r\n";

            //sqlファイル内SQLを実行
            strBatch += $"echo %date%_%time%_%DBNAME%_download_scangroup\r\n";
            strBatch += $"psql --quiet -h {strDBHost} -p {intDBPort} -U postgres -d %DBNAME% -f %OUTPUTPATH%\\{strSQLFileName}\r\n";// --command \"{strcopycommand}\";\r\n";


#if DEBUG
            strBatch += $"pause\r\n";
#endif
            System.IO.StreamWriter sw = null;
            try
            {
                //psqlバッチに食わせるsqlファイル
                sw = new System.IO.StreamWriter(strOutputPath + $"\\{strSQLFileName}");
                sw.WriteLine(strsql_scangroup);
                sw.Close();

                //psqlバッチ
                sw = new System.IO.StreamWriter(strOutputPath + $"\\{strDBName}_SCANGROUP_upload_{DateTime.Now.ToString("yyyyMMddHHmmss")}.bat");
                sw.WriteLine(strBatch);
                sw.Close();

                RunBatch(strOutputPath + $"\\{strDBName}_SCANGROUP_upload_{DateTime.Now.ToString("yyyyMMddHHmmss")}.bat");

            }
            catch (Exception ex)
            {
                ErrMessage(GetCurrentMethod().Name + "\r\n" + ex.Message);
            }
            finally
            {


            }

        }
        #endregion

        #region バッチ実行
        /// <summary>
        /// 作ったバッチを実行
        /// </summary>
        /// <param name="strFileName"></param>
        private static void RunBatch(string strFileName)
        {


            System.Diagnostics.ProcessStartInfo si = new System.Diagnostics.ProcessStartInfo(strFileName);
            System.Diagnostics.Process p = new System.Diagnostics.Process();


            //#if DEBUG
            //            si.UseShellExecute = true;
            //            si.CreateNoWindow = false;
            //#else
            //si.UseShellExecute = false;
            //si.CreateNoWindow = true;
            //#endif
            p.StartInfo = si;

            p.Start();
            // p.WaitForExit();



        }
        #endregion


        /// <summary>
        /// aws更新が終わったScanレコードを削除対象にする処理
        /// 実際には自動削除しない。将来的にするかも
        /// </summary>
        public static void UpdateScanStatusFinish()
        {
            Npgsql.NpgsqlConnection cn = new Npgsql.NpgsqlConnection();
            Npgsql.NpgsqlCommand cmd = new Npgsql.NpgsqlCommand();
            Npgsql.NpgsqlDataAdapter da = new Npgsql.NpgsqlDataAdapter();

            cn.ConnectionString =
                        $"Server={dsSetting.Tables["localocrdb"].Rows[0]["dbserver"].ToString()};" +
                        $"port={dsSetting.Tables["localocrdb"].Rows[0]["dbport"].ToString()};" +
                        $"database={strDBName};" +
                        $"user id={dsSetting.Tables["localocrdb"].Rows[0]["dbuser"].ToString()};" +
                        $"password={dsSetting.Tables["localocrdb"].Rows[0]["dbpass"].ToString()};" +
                        $"CommandTimeout = 30; timeout = 30; connectionlifetime = 120; ";



            Npgsql.NpgsqlTransaction tran = null;
            try
            {
                cn.Open();
                tran = cn.BeginTransaction();
                cmd.Connection = cn;


                foreach (ScanforAWSUpdate s in lstScanid)
                {
                    //アップロードしたscanidはステータス4にして、最初のスキャンID取得時にダブって取らないようにする
                    string strsql = $"update scan set status={(int)SCAN_STATUS.UPload済み削除対象} where status={(int)SCAN_STATUS.OCR済み} and sid={s.scanid}";
                    cmd.CommandText = strsql;
                    cmd.ExecuteNonQuery();
                }
                tran.Commit();

            }
            catch (Exception ex)
            {
                ErrMessage(GetCurrentMethod().Name + "\r\n" + ex.Message);
                tran.Rollback();
            }
            finally
            {
                cn.Close();
            }

        }
    }
}

