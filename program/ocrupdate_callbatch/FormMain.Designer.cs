﻿namespace ocrupdate_callbatch
{
    partial class FormMain
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.buttonDownLoad = new System.Windows.Forms.Button();
            this.timerDownLoad = new System.Windows.Forms.Timer(this.components);
            this.labeConn = new System.Windows.Forms.Label();
            this.labelTimer = new System.Windows.Forms.Label();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.numericUpDownInterval = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.timerStartDownload = new System.Windows.Forms.Timer(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.buttonUpdAWS = new System.Windows.Forms.Button();
            this.timerAWSUpdate = new System.Windows.Forms.Timer(this.components);
            this.buttonSetting = new System.Windows.Forms.Button();
            this.timerStartUpload = new System.Windows.Forms.Timer(this.components);
            this.dgvDownLoad = new System.Windows.Forms.DataGridView();
            this.dgvUpload = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownInterval)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDownLoad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUpload)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonDownLoad
            // 
            this.buttonDownLoad.Location = new System.Drawing.Point(69, 33);
            this.buttonDownLoad.Margin = new System.Windows.Forms.Padding(4);
            this.buttonDownLoad.Name = "buttonDownLoad";
            this.buttonDownLoad.Size = new System.Drawing.Size(144, 52);
            this.buttonDownLoad.TabIndex = 0;
            this.buttonDownLoad.Text = "ダウンロード開始";
            this.buttonDownLoad.UseVisualStyleBackColor = true;
            this.buttonDownLoad.Click += new System.EventHandler(this.buttonDownLoad_Click);
            // 
            // timerDownLoad
            // 
            this.timerDownLoad.Tick += new System.EventHandler(this.timerDownLoad_Tick);
            // 
            // labeConn
            // 
            this.labeConn.AutoSize = true;
            this.labeConn.Location = new System.Drawing.Point(66, 132);
            this.labeConn.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labeConn.Name = "labeConn";
            this.labeConn.Size = new System.Drawing.Size(45, 16);
            this.labeConn.TabIndex = 1;
            this.labeConn.Text = "label1";
            // 
            // labelTimer
            // 
            this.labelTimer.AutoSize = true;
            this.labelTimer.Location = new System.Drawing.Point(66, 103);
            this.labelTimer.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTimer.Name = "labelTimer";
            this.labelTimer.Size = new System.Drawing.Size(45, 16);
            this.labelTimer.TabIndex = 2;
            this.labelTimer.Text = "label2";
            // 
            // listBox1
            // 
            this.listBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listBox1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(421, 28);
            this.listBox1.Margin = new System.Windows.Forms.Padding(4);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(407, 355);
            this.listBox1.TabIndex = 3;
            // 
            // numericUpDownInterval
            // 
            this.numericUpDownInterval.Location = new System.Drawing.Point(147, 156);
            this.numericUpDownInterval.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDownInterval.Maximum = new decimal(new int[] {
            6000,
            0,
            0,
            0});
            this.numericUpDownInterval.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownInterval.Name = "numericUpDownInterval";
            this.numericUpDownInterval.Size = new System.Drawing.Size(48, 22);
            this.numericUpDownInterval.TabIndex = 4;
            this.numericUpDownInterval.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(203, 161);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(23, 16);
            this.label3.TabIndex = 5;
            this.label3.Text = "秒";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(66, 161);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 16);
            this.label4.TabIndex = 6;
            this.label4.Text = "監視間隔";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(241, 33);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(129, 16);
            this.label5.TabIndex = 6;
            this.label5.Text = "Download作動時刻";
            // 
            // timerStartDownload
            // 
            this.timerStartDownload.Enabled = true;
            this.timerStartDownload.Interval = 1000;
            this.timerStartDownload.Tick += new System.EventHandler(this.timerStartDownload_Tick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(241, 229);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 16);
            this.label1.TabIndex = 8;
            this.label1.Text = "Upload作動時刻";
            // 
            // buttonUpdAWS
            // 
            this.buttonUpdAWS.Location = new System.Drawing.Point(69, 229);
            this.buttonUpdAWS.Margin = new System.Windows.Forms.Padding(4);
            this.buttonUpdAWS.Name = "buttonUpdAWS";
            this.buttonUpdAWS.Size = new System.Drawing.Size(144, 52);
            this.buttonUpdAWS.TabIndex = 0;
            this.buttonUpdAWS.Text = "AWS更新開始";
            this.buttonUpdAWS.UseVisualStyleBackColor = true;
            this.buttonUpdAWS.Click += new System.EventHandler(this.buttonAWSUpdate_Click);
            // 
            // timerAWSUpdate
            // 
            this.timerAWSUpdate.Interval = 1000;
            this.timerAWSUpdate.Tick += new System.EventHandler(this.timerAWSUpdate_Tick);
            // 
            // buttonSetting
            // 
            this.buttonSetting.Location = new System.Drawing.Point(69, 350);
            this.buttonSetting.Name = "buttonSetting";
            this.buttonSetting.Size = new System.Drawing.Size(122, 33);
            this.buttonSetting.TabIndex = 10;
            this.buttonSetting.Text = "設定";
            this.buttonSetting.UseVisualStyleBackColor = true;
            this.buttonSetting.Click += new System.EventHandler(this.buttonSetting_Click);
            // 
            // timerStartUpload
            // 
            this.timerStartUpload.Enabled = true;
            this.timerStartUpload.Interval = 1000;
            this.timerStartUpload.Tick += new System.EventHandler(this.timerStartUpload_Tick);
            // 
            // dgvDownLoad
            // 
            this.dgvDownLoad.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDownLoad.Location = new System.Drawing.Point(244, 52);
            this.dgvDownLoad.Name = "dgvDownLoad";
            this.dgvDownLoad.RowHeadersVisible = false;
            this.dgvDownLoad.Size = new System.Drawing.Size(125, 109);
            this.dgvDownLoad.TabIndex = 11;
            // 
            // dgvUpload
            // 
            this.dgvUpload.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvUpload.Location = new System.Drawing.Point(244, 248);
            this.dgvUpload.Name = "dgvUpload";
            this.dgvUpload.RowHeadersVisible = false;
            this.dgvUpload.Size = new System.Drawing.Size(125, 109);
            this.dgvUpload.TabIndex = 11;
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(907, 395);
            this.Controls.Add(this.dgvUpload);
            this.Controls.Add(this.dgvDownLoad);
            this.Controls.Add(this.buttonSetting);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.numericUpDownInterval);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.labelTimer);
            this.Controls.Add(this.labeConn);
            this.Controls.Add(this.buttonUpdAWS);
            this.Controls.Add(this.buttonDownLoad);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FormMain";
            this.Text = "OCR_UpdAWS";
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownInterval)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDownLoad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUpload)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonDownLoad;
        private System.Windows.Forms.Timer timerDownLoad;
        private System.Windows.Forms.Label labeConn;
        private System.Windows.Forms.Label labelTimer;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.NumericUpDown numericUpDownInterval;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Timer timerStartDownload;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonUpdAWS;
        private System.Windows.Forms.Timer timerAWSUpdate;
        private System.Windows.Forms.Button buttonSetting;
        private System.Windows.Forms.Timer timerStartUpload;
        private System.Windows.Forms.DataGridView dgvDownLoad;
        private System.Windows.Forms.DataGridView dgvUpload;
    }
}

