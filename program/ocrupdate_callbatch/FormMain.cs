﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ocrupdate_callbatch
{
    //保険者名一覧を取得して、コマンドに渡すexe
    public partial class FormMain : Form
    {
        bool flgDown = false;
        bool flgUp = false;

        public enum SCAN_STATUS { NULL = 0, SCANING = 1, 取込済み = 2, OCR済み = 3, }

        public List<string> lstInsurer = new List<string>();

        static string strDBHost = string.Empty;
        static int intDBPort = 0;
        static string strDBUser = string.Empty;
        static string strDBPass = string.Empty;
        static string strDBName = string.Empty;
        static int intTimeOut = 0;

        /// <summary>
        /// 保険者テーブル取得
        /// </summary>
        DataTable dtInsurerDownload = new DataTable();
        DataTable dtInsurerUpload = new DataTable();

        /// <summary>
        /// 設定ファイル用
        /// </summary>
        DataSet dsSettings = new DataSet();

        /// <summary>
        /// 保険者ループカウンタ
        /// </summary>
        int dtInsurerRowCounter = 0;


        /// <summary>
        /// エラーログ用フォルダ
        /// </summary>
        string strErrPath = $"{Environment.CurrentDirectory}\\errlog";

        public static string strBatchSaveFolderPerLoop = string.Empty;

        public FormMain()
        {
            InitializeComponent();

            if (!getSettings()) return;
            if (!getinsurerFromAWS()) return;

            timerStartDownload.Enabled = true;


            //エラーログ用フォルダ作成            
            if (!System.IO.Directory.Exists(strErrPath)) System.IO.Directory.CreateDirectory(strErrPath);


        }



        private bool getSettings()
        {
            if (!System.IO.File.Exists("settings.xml"))
            {
                MessageBox.Show("settings.xmlが見つかりません");
                return false;
            }
            dsSettings.ReadXml("settings.xml");

            //バッチ開始時刻取得
            if (!settimes()) return false;

            return true;
        }


        /// <summary>
        /// xmlよりバッチ開始時刻取得
        /// </summary>
        /// <returns></returns>
        private bool settimes()
        {

            //List<string> lstDownTime;// = new List<string>();

            try
            {
                List<string> lstDownTime = dsSettings.Tables["settings"].Rows[0]["downloadTime"].ToString().Split(',').ToList<string>();

                DataTable dtDownloadTime = new DataTable();
                dtDownloadTime.Columns.Add("downloadtime");

                foreach (string s in lstDownTime)
                {
                    DataRow dr = dtDownloadTime.NewRow();
                    dr[0] = s;
                    dtDownloadTime.Rows.Add(dr);
                }

                dgvDownLoad.DataSource = dtDownloadTime;


                List<string> lstUpTime = dsSettings.Tables["settings"].Rows[0]["uploadTime"].ToString().Split(',').ToList<string>();
                DataTable dtUploadTime = new DataTable();
                dtUploadTime.Columns.Add("uploadtime");

                foreach (string s in lstUpTime)
                {
                    DataRow dr = dtUploadTime.NewRow();
                    dr[0] = s;
                    dtUploadTime.Rows.Add(dr);
                }

                dgvUpload.DataSource = dtUploadTime;


                return true;
            }
            catch(Exception ex)
            {
                ErrMessage(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\rn" + ex.Message);
                return false;
            }

        }

        public static void ErrMessage(string strDisp)
        {
            System.Windows.Forms.MessageBox.Show(strDisp);
        }

        /// <summary>
        /// AWSからダウンロード処理の開始ボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonDownLoad_Click(object sender, EventArgs e)
        {
            //2022/05/27ボタンを押すたびにdbを再ロード
            getinsurerFromAWS();

            if (!flgDown)
            {

                strBatchSaveFolderPerLoop = DateTime.Now.ToString("yyyyMMdd_HHmmss");

                labelTimer.Text = "timer=true";
                timerDownLoad.Interval = int.Parse(numericUpDownInterval.Value.ToString()) * 1000;//秒
                timerDownLoad.Enabled = true;
                timerDownLoad.Start();

                flgDown = true;
                buttonDownLoad.Text = "ダウンロード停止";
                buttonDownLoad.BackColor = Color.DarkSalmon;
            }
            else
            {
                labelTimer.Text = "timer=false";
                timerDownLoad.Stop();
                flgDown = false;
                buttonDownLoad.Text = "ダウンロード開始";
                buttonDownLoad.BackColor = System.Drawing.SystemColors.Control;
            }
            
        }

        /// <summary>
        /// jyuseiDBより保険者取得
        /// </summary>
        private bool getinsurerFromAWS()
        {
            Npgsql.NpgsqlConnection cn = new Npgsql.NpgsqlConnection();
            Npgsql.NpgsqlCommand cmd = new Npgsql.NpgsqlCommand();
            Npgsql.NpgsqlDataAdapter da = new Npgsql.NpgsqlDataAdapter();

            labeConn.Text = cn.State.ToString();
        
            try
            {
                //ダウンロードしてくる方の保険者テーブル
                string strTableName = "download_db";
                cn.ConnectionString =
                    $"Server={dsSettings.Tables[strTableName].Rows[0]["dbserver"].ToString()};" +
                    $"port={dsSettings.Tables[strTableName].Rows[0]["dbport"].ToString()};" +
                    $"database=jyusei;" +
                    $"user id={dsSettings.Tables[strTableName].Rows[0]["dbuser"].ToString()};" +
                    $"password={dsSettings.Tables[strTableName].Rows[0]["dbpass"].ToString()};" +
                    $"CommandTimeout=10;timeout=10;connectionlifetime=3;sslmode=require";


                cn.Open();

                labeConn.Text = cn.State.ToString();

                cmd.CommandText = "select * from insurer where enabled=true order by scanorder";
                cmd.Connection = cn;
                da.SelectCommand = cmd;
                da.Fill(dtInsurerDownload);

                cn.Close();



                //アップロード先の保険者テーブル
                strTableName = "upload_db";
                cn.ConnectionString =
                    $"Server={dsSettings.Tables[strTableName].Rows[0]["dbserver"].ToString()};" +
                    $"port={dsSettings.Tables[strTableName].Rows[0]["dbport"].ToString()};" +
                    $"database=jyusei;" +
                    $"user id={dsSettings.Tables[strTableName].Rows[0]["dbuser"].ToString()};" +
                    $"password={dsSettings.Tables[strTableName].Rows[0]["dbpass"].ToString()};" +
                    $"CommandTimeout=10;timeout=10;connectionlifetime=3;sslmode=require";


                cn.Open();

                labeConn.Text = cn.State.ToString();

                cmd.CommandText = "select * from insurer where enabled=true order by scanorder";
                cmd.Connection = cn;
                da.SelectCommand = cmd;
                da.Fill(dtInsurerUpload);

                return true;
            }

            catch (Exception ex)
            {
                ErrMessage(System.Reflection.MethodBase.GetCurrentMethod().Name + ex.Message);
                return false;
            }
            finally
            {
                cmd.Dispose();
                cn.Close();
                
            }
        }


        /// <summary>
        /// ダウンロード処理（dtInsurer（保険者テーブル）をループ）
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timerDownLoad_Tick(object sender, EventArgs e)
        {
         
            
            Task.Factory.StartNew(() =>
            {

                strDBName = dtInsurerDownload.Rows[dtInsurerRowCounter]["dbname"].ToString();
                strDBHost = dtInsurerDownload.Rows[dtInsurerRowCounter]["dbserver"].ToString();
                intDBPort = int.Parse(dtInsurerDownload.Rows[dtInsurerRowCounter]["dbport"].ToString());
                strDBPass = dtInsurerDownload.Rows[dtInsurerRowCounter]["dbpass"].ToString();
                strDBUser = dtInsurerDownload.Rows[dtInsurerRowCounter]["dbuser"].ToString();


                listBox1.Invoke(new Action(()=>
                {
                    listBox1.Items.Insert(0, $"down start {strDBName.PadRight(20, ' ')} {DateTime.Now.ToString()}");
                }));


                System.Diagnostics.ProcessStartInfo si = new System.Diagnostics.ProcessStartInfo();

                si.FileName = "ocrupdate_batch.exe";
                si.Arguments = $"{strDBHost} {strDBName} {intDBPort} {strDBUser} {strDBPass} {strErrPath} {strBatchSaveFolderPerLoop}";

//#if !DEBUG
//                si.UseShellExecute = false;
//                si.CreateNoWindow = true;
//#endif

                System.Diagnostics.Process p = new System.Diagnostics.Process();
                p.StartInfo = si;
                if (!p.Start())
                {
                    MessageBox.Show("ng");
                    return;
                }

                dtInsurerRowCounter++;

                p.WaitForExit();

                //バックグラウンドの処理が終わったら終了の表示
                if (p.HasExited)
                {
                    listBox1.Invoke(new Action(() =>
                    {
                        listBox1.Items.Insert(0, $"down finish {strDBName.PadRight(20, ' ')} {DateTime.Now.ToString()}");
                    }));
                }

                if (dtInsurerRowCounter >= dtInsurerDownload.Rows.Count)
                {
                    //timerStartUpload.Enabled= true;//アップロード側時刻確認タイマー復帰
                    //timerStartDownload.Enabled = true;//保険者を1周したら時刻確認タイマーを復帰                    
                    buttonDownLoad_Click(sender, e);//保険者を1周したらボタンを押してダウンロードタイマーを止める
                    dtInsurerRowCounter=0;
                    return;
                }
            });
            
            
        }


    


        /// <summary>
        /// OCRした結果をAWSに戻す（Upload)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonAWSUpdate_Click(object sender, EventArgs e)
        {
            
            if (!flgUp)
            {
                strBatchSaveFolderPerLoop = DateTime.Now.ToString("yyyyMMdd_HHmmss");

                labelTimer.Text = "timer=true";
                timerAWSUpdate.Interval = int.Parse(numericUpDownInterval.Value.ToString()) * 1000;//秒
                timerAWSUpdate.Enabled = true;
                timerAWSUpdate.Start();

                flgUp = true;
                buttonUpdAWS.Text = "AWS更新停止";
                buttonUpdAWS.BackColor = Color.DarkSalmon;
            }
            else
            {
                labelTimer.Text = "timer=false";
                timerAWSUpdate.Stop();
                flgUp = false;
                buttonUpdAWS.Text = "AWS更新開始";
                buttonUpdAWS.BackColor = System.Drawing.SystemColors.Control;
            }

        }


     
        /// <summary>
        /// ocrしたデータをAWSに更新する処理をdtInsurer（保険者テーブル）の行数ループ
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timerAWSUpdate_Tick(object sender, EventArgs e)
        {
           

            Task.Factory.StartNew(() =>
            {

                strDBName = dtInsurerUpload.Rows[dtInsurerRowCounter]["dbname"].ToString();
                strDBHost = dtInsurerUpload.Rows[dtInsurerRowCounter]["dbserver"].ToString();
                intDBPort = int.Parse(dtInsurerUpload.Rows[dtInsurerRowCounter]["dbport"].ToString());
                strDBPass = dtInsurerUpload.Rows[dtInsurerRowCounter]["dbpass"].ToString();
                strDBUser = dtInsurerUpload.Rows[dtInsurerRowCounter]["dbuser"].ToString();

                listBox1.Invoke(new Action(() =>
                {
                    listBox1.Items.Insert(0, $"up start {strDBName.PadRight(20, ' ')} {DateTime.Now.ToString()}");
                }));

                System.Diagnostics.ProcessStartInfo si = new System.Diagnostics.ProcessStartInfo();

                si.FileName = "ocrupdate_batch_upload.exe";
                si.Arguments = $"{strDBHost} {strDBName} {intDBPort} {strDBUser} {strDBPass} {strErrPath} {strBatchSaveFolderPerLoop}";

//#if !DEBUG
//                si.UseShellExecute = false;
//                si.CreateNoWindow = true;
//#endif

                System.Diagnostics.Process p = new System.Diagnostics.Process();

                p.StartInfo = si;
                if (!p.Start())
                {
                    MessageBox.Show("ng");
                    return;
                }
                
                p.WaitForExit();

                //バックグラウンドの処理が終わったら終了の表示
                if (p.HasExited)
                {
                    listBox1.Invoke(new Action(() =>
                    {
                        listBox1.Items.Insert(0, $"up finish {strDBName.PadRight(20, ' ')} {DateTime.Now.ToString()}");
                    }));
                }

                dtInsurerRowCounter++;
                if (dtInsurerRowCounter >= dtInsurerUpload.Rows.Count)
                {
                    //timerStartDownload.Enabled = true;//ダウンロード時刻確認タイマー復帰
                    //timerStartUpload.Enabled = true;//保険者を1周したら時刻確認タイマーを復帰                    
                    buttonAWSUpdate_Click(sender, e);//保険者を1周したらボタンを押してアップロードタイマーを止める
                    dtInsurerRowCounter = 0;
                    return;
                    
                }

            });

        }

        private void buttonSetting_Click(object sender, EventArgs e)
        {
            Task.Factory.StartNew(() =>
            {
                FormSettings frmSetting = new FormSettings();
                frmSetting.ShowDialog();
            });
        }



        /// <summary>
        /// 固定開始時間
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timerStartDownload_Tick(object sender, EventArgs e)
        {
            if (dgvDownLoad.DataSource == null) return;
            if (dgvDownLoad.Rows.Count == 0) return;
            if (timerDownLoad.Enabled) return;
            foreach (DataGridViewRow dr in dgvDownLoad.Rows)
            {
                if (dr.Cells[0].Value == null) return;
                if (DateTime.Now.ToString("HH:mm")==dr.Cells[0].Value.ToString())
                    buttonDownLoad_Click(sender, e);//時刻になったらボタンを押す行動
                continue;
            }
           


            //if (maskedTextBoxTime.Text == "  時  分") return;
            //if (timerDownLoad.Enabled) return;

            //if (DateTime.Now.ToString("HH時mm分") == maskedTextBoxTime.Text)
            //{

            //    buttonDownLoad_Click(sender, e);//時刻になったらボタンを押す行動
            //    //timerStartDownload.Enabled = false;//一旦止める

            //}
        }

        
        private void timerStartUpload_Tick(object sender, EventArgs e)
        {


            if (dgvUpload.DataSource == null) return;
            if (dgvUpload.Rows.Count == 0) return;
            if (timerAWSUpdate.Enabled) return;
            foreach (DataGridViewRow dr in dgvUpload.Rows)
            {
                if (dr.Cells[0].Value == null) return;
                if (DateTime.Now.ToString("HH:mm") == dr.Cells[0].Value.ToString())
                    buttonAWSUpdate_Click(sender, e);//時刻になったらボタンを押す行動
                continue;
            }



            //if (maskedTextBoxUpload.Text == "  時  分") return;
            //if (timerAWSUpdate.Enabled) return;
            
            
            //if (DateTime.Now.ToString("HH時mm分") == maskedTextBoxUpload.Text)
            //{
                
            //    buttonAWSUpdate_Click(sender, e);//時刻になったらボタンを押す行動
            //    //timerStartUpload.Enabled = false;//一旦止める

            //}
        }

    }
}
