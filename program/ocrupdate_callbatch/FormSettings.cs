﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ocrupdate_callbatch
{
    public partial class FormSettings : Form
    {
        DataSet dsSettings = new DataSet();
        DataTable dtDisp = new DataTable();

        public FormSettings()
        {
            InitializeComponent();

            dsSettings.ReadXml("settings.xml");

            loadSettingTableName();
        }

        public static void Message(string strMethodName,string strDisp)
        {
            System.Windows.Forms.MessageBox.Show($"{strMethodName}\r\n{strDisp}");
        }

        private void loadSettingTableName()
        {
            foreach (DataTable dt in dsSettings.Tables)
            { 
                comboBoxTable.Items.Add(dt.TableName);
            }

            
        }

        private bool loadSettingsTable()
        {            
            try
            {
                dtDisp = dsSettings.Tables[comboBoxTable.Text];
                dataGridViewSettings.DataSource = dtDisp;
                dataGridViewSettings.AutoResizeColumns();
                
                return true;
            }
            catch(Exception ex)
            {
                Message(System.Reflection.MethodBase.GetCurrentMethod().Name , ex.Message);
                return false;
            }
        }

        private void comboBoxTable_SelectionChangeCommitted(object sender, EventArgs e)
        {
            loadSettingsTable();
        }

        private void buttonUpd_Click(object sender, EventArgs e)
        {
            try
            {
                if (dtDisp.Rows[0].RowState != DataRowState.Unchanged)
                {
                    dsSettings.WriteXml("settings.xml");
                }
                Message("","正常終了。このアプリケーションを再起動してください");
                this.Close();
            }
            catch (Exception ex)
            {
                Message(System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message);
                
            }
            finally
            {

            }
        }
    }
}
