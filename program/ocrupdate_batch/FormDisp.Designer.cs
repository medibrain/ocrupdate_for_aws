﻿namespace ocrupdate_batch
{
    partial class FormDisp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelins = new System.Windows.Forms.Label();
            this.labelscan = new System.Windows.Forms.Label();
            this.labelapp = new System.Windows.Forms.Label();
            this.labelscangroup = new System.Windows.Forms.Label();
            this.labelloadscanid = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labelins
            // 
            this.labelins.AutoSize = true;
            this.labelins.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.labelins.Location = new System.Drawing.Point(112, 28);
            this.labelins.Name = "labelins";
            this.labelins.Size = new System.Drawing.Size(43, 13);
            this.labelins.TabIndex = 0;
            this.labelins.Text = "保険者";
            // 
            // labelscan
            // 
            this.labelscan.AutoSize = true;
            this.labelscan.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.labelscan.Location = new System.Drawing.Point(112, 59);
            this.labelscan.Name = "labelscan";
            this.labelscan.Size = new System.Drawing.Size(86, 13);
            this.labelscan.TabIndex = 1;
            this.labelscan.Text = "Scan.bat生成中";
            // 
            // labelapp
            // 
            this.labelapp.AutoSize = true;
            this.labelapp.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.labelapp.Location = new System.Drawing.Point(112, 122);
            this.labelapp.Name = "labelapp";
            this.labelapp.Size = new System.Drawing.Size(80, 13);
            this.labelapp.TabIndex = 2;
            this.labelapp.Text = "App.bat生成中";
            // 
            // labelscangroup
            // 
            this.labelscangroup.AutoSize = true;
            this.labelscangroup.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.labelscangroup.Location = new System.Drawing.Point(112, 186);
            this.labelscangroup.Name = "labelscangroup";
            this.labelscangroup.Size = new System.Drawing.Size(115, 13);
            this.labelscangroup.TabIndex = 3;
            this.labelscangroup.Text = "ScanGroup.bat生成中";
            // 
            // labelloadscanid
            // 
            this.labelloadscanid.AutoSize = true;
            this.labelloadscanid.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.labelloadscanid.Location = new System.Drawing.Point(112, 90);
            this.labelloadscanid.Name = "labelloadscanid";
            this.labelloadscanid.Size = new System.Drawing.Size(79, 13);
            this.labelloadscanid.TabIndex = 1;
            this.labelloadscanid.Text = "ScanID取得中";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.label1.Location = new System.Drawing.Point(112, 151);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "App取得中";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.label2.Location = new System.Drawing.Point(112, 216);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "ScanGroup更新";
            // 
            // FormDisp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(362, 296);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.labelscangroup);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.labelapp);
            this.Controls.Add(this.labelloadscanid);
            this.Controls.Add(this.labelscan);
            this.Controls.Add(this.labelins);
            this.Name = "FormDisp";
            this.Text = "FormDisp";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Label labelins;
        public System.Windows.Forms.Label labelscan;
        public System.Windows.Forms.Label labelapp;
        public System.Windows.Forms.Label labelscangroup;
        public System.Windows.Forms.Label labelloadscanid;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label label2;
    }
}