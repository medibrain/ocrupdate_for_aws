﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Reflection.MethodBase;

namespace ocrupdate_batch
{
    //保険者名を貰って、バッチファイルを作成
    class Program
    {
        

        public static string strDBHost = string.Empty;
        public static int intDBPort = 0;
        public static string strDBUser = string.Empty;
        public static string strDBPass = string.Empty;
        public static string strDBName = string.Empty;
        public static int intTimeOut = 0;
        
        #region バッチ作成用変数
        public  enum SCAN_STATUS { NULL = 0, SCANING = 1, 取込済み = 2, OCR済み = 3, }
        public static string strEncode = "utf8";//sqlファイルの文字コード
                                                     //string strEncode = "shift-jis";//sqlファイルの文字コード
        public static string strPsqlPath = string.Empty;//"C:\\program files\\postgresql\\11\\bin";//psql.exeのあるフォルダパス
        public static string strLocalServer = string.Empty;//"--host localhost --port 5440 --username postgres";//OCR機のDB接続情報。パスワードはパスワードファイル前提

        #endregion

        public static string strOutputPath = string.Empty;//バッチ、sqlファイルの作成場所

        public static System.Data.DataSet dsSetting = new System.Data.DataSet();//settings用dataset

        public static string strErrpath = string.Empty;//エラーログ用フォルダ
        public static string strBatchSaveFolderPerLoop = string.Empty;


        static string[] args;

        /// <summary>
        /// 引数は接続文字列の各値
        /// </summary>
        /// <param name="_args"></param>
        static void Main(string[] _args)
        {
            args = _args;
            //Task.Factory.StartNew(() =>
            //{

#if DEBUG
            System.Windows.Forms.MessageBox.Show("start");
#endif

            strDBHost = args[0];
            strDBName = args[1];
            intDBPort = int.Parse(args[2]);
            strDBUser = args[3];
            strDBPass = args[4];
            strErrpath = args[5];
            strBatchSaveFolderPerLoop = args[6];

            if (!LoadSetting()) return;

            //フォルダなかったら作成
            if (!System.IO.Directory.Exists(strOutputPath)) System.IO.Directory.CreateDirectory(strOutputPath);

            CreateAndRunBatch();
            
        //    });
            
        }

        public static void ErrMessage(string strDisp)
        {

            System.IO.StreamWriter sw = new System.IO.StreamWriter($"{strErrpath}\\errlog.txt", true);
            sw.WriteLine($"{DateTime.Now.ToString("yyyyMMdd_HHmmss")}:{strDisp}");
            sw.Close();
            //System.Windows.Forms.MessageBox.Show(strDisp,"ocrupdate_batch");
        }


        private static bool LoadSetting()
        {
            

            try
            {
                dsSetting.ReadXml("settings.xml");
             
                strOutputPath = $"{dsSetting.Tables[0].Rows[0]["DownloadBatchOutputPath"].ToString()}\\{strBatchSaveFolderPerLoop}";
                strPsqlPath = $"{dsSetting.Tables[0].Rows[0]["psqlPath"].ToString()}";

                strLocalServer =
                    $"--host {dsSetting.Tables["localocrdb"].Rows[0]["dbserver"].ToString()} " +
                    $"--port {dsSetting.Tables["localocrdb"].Rows[0]["dbport"].ToString()} " +
                    $"--username {dsSetting.Tables["localocrdb"].Rows[0]["dbuser"].ToString()} ";


                return true;
            }
            catch(Exception ex)
            {
                ErrMessage(GetCurrentMethod().Name + ex.Message);
                return false;
            }
           
        }

        private static bool CreateAndRunBatch()
        {
         
          
            try
            {
                CreateBatchFile_scan();

                List<int> lstscan = getScanIDFromLocalOCRDB();
                CreateBatchFile_App(lstscan);
                CreateBatchFile_scangroup(lstscan);
                
                return true;
            }
            
            catch(Exception ex)
            {
                ErrMessage(GetCurrentMethod().Name + ex.Message);
                return false;
            }
       
         }


        #region Scanテーブル更新バッチ
        private static void CreateBatchFile_scan()
        {

            string strBatch = string.Empty;
            string strsql_scan =
                $"select * FROM scan WHERE status={(int)SCAN_STATUS.取込済み} ORDER BY sid";


            string strcopycommand = $"\\copy ({strsql_scan}) TO %OUTPUTPATH%\\%FILENAME%";

            string strTruncatesql = "truncate table ocrimport_scan";
            string strCopysql = $"\\copy ocrimport_scan from %OUTPUTPATH%\\%FILENAME%";
            //string strCopysql = $"\\copy scan from %OUTPUTPATH%\\%FILENAME%";
            string strInsertNoConflict = $"insert into scan select * from ocrimport_scan on conflict do nothing";

            strBatch += $"@echo off\r\n";
            strBatch += $"set PGCLIENTENCODING={strEncode}\r\n";
            strBatch += $"set DBNAME={strDBName}\r\n";
            strBatch += $"set FILENAME={strDBName}_scan.sql\r\n";
            strBatch += $"set OUTPUTPATH={strOutputPath}\r\n";
            strBatch += $"mkdir %OUTPUTPATH%\r\n";
            strBatch += $"cd /d {strPsqlPath}\r\n";

            //レコードを選択しsqlファイルとして保存
            strBatch += $"echo %date%_%time%_%DBNAME%_download_scan\r\n";
            strBatch += $"psql -h {strDBHost} -p {intDBPort} -U postgres -d %DBNAME% --command \"{strcopycommand}\";\r\n";

           
            //ocr機のocr用一時テーブルをtruncate
            strBatch += $"echo %date%_%time%_%DBNAME%_truncate ocrimport_scan\r\n";
            strBatch += $"psql {strLocalServer} --dbname %DBNAME% --command \"{strTruncatesql}\";\r\n";

            //ocr機にダウンロードしたsqlをインポート
            strBatch += $"echo %date%_%time%_%DBNAME%_copy to ocrimport_scan\r\n";
            strBatch += $"psql {strLocalServer} --dbname %DBNAME% --command \"{strCopysql}\";\r\n";

            //一時テーブルからscanにコピー。重複は無視する
            strBatch += $"echo %date%_%time%_%DBNAME%_copy ocrimport_scan to scan \r\n";
            strBatch += $"psql {strLocalServer} --dbname %DBNAME% --command \"{strInsertNoConflict}\";\r\n";


#if DEBUG
            strBatch += $"pause\r\n";
#endif
            System.IO.StreamWriter sw = null;
            try
            {
                sw = new System.IO.StreamWriter(strOutputPath + $"\\{strDBName}_SCAN_{DateTime.Now.ToString("yyyyMMddHHmmss")}.bat");
                sw.WriteLine(strBatch);
                sw.Close();

                RunBatch(strOutputPath + $"\\{strDBName}_SCAN_{DateTime.Now.ToString("yyyyMMddHHmmss")}.bat");
            }
            catch (Exception ex)
            {
                ErrMessage(GetCurrentMethod().Name + ex.Message);
            }
            finally
            {
                
            }


        }
        #endregion

        #region scanid取得
        /// <summary>
        /// ローカルに落としたScanテーブルのScanIDを取得し、Application、Scanテーブルをダウンロードするキーとする
        /// </summary>
        /// <returns></returns>
        public static List<int> getScanIDFromLocalOCRDB()
        {
            Npgsql.NpgsqlConnection cn = new Npgsql.NpgsqlConnection();
            Npgsql.NpgsqlCommand cmd = new Npgsql.NpgsqlCommand();
           //Npgsql.NpgsqlDataAdapter da = new Npgsql.NpgsqlDataAdapter();


            List<int> lstScanid = new List<int>();

            //cn.ConnectionString =
            //             $"Server=localhost;Port=5440;Database={strDBName};User Id=postgres;" +
            //             $"Password=pass;CommandTimeout=3;timeout=3;connectionlifetime=3;";
                        
            cn.ConnectionString =
                $"Server={dsSetting.Tables["localocrdb"].Rows[0]["dbserver"].ToString()};" +
                $"port={dsSetting.Tables["localocrdb"].Rows[0]["dbport"].ToString()};" +
                $"database={strDBName};" +
                $"user id={dsSetting.Tables["localocrdb"].Rows[0]["dbuser"].ToString()};" +
                $"password={dsSetting.Tables["localocrdb"].Rows[0]["dbpass"].ToString()};" +
                $"CommandTimeout = 10; timeout = 10; connectionlifetime = 10; ";
            //2021/01/15 既存の接続はリモートホストに強制的に切断されました　のエラーが出るので調整してみる
            //$"CommandTimeout = 3; timeout = 3; connectionlifetime = 3; ";


            string strsql = $"select sid from scan where status={(int)SCAN_STATUS.取込済み}";
           

            try
            {
                cn.Open();

                cmd.Connection = cn;
                cmd.CommandText = strsql;

                Npgsql.NpgsqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    if (!int.TryParse(dr[0].ToString(), out int res)) continue;
                    lstScanid.Add(res);

                }
                dr.Close();



                return lstScanid;

            }
            catch (Exception ex)
            {
                ErrMessage(GetCurrentMethod().Name + "\r\n"+ex.Message);
                return lstScanid;            
            }
            finally
            {
                cmd.Dispose();
                cn.Close();
            }


        }
        #endregion

        #region app取得バッチ
        /// <summary>
        /// awsからapplicationレコードをダウンロードしてくるバッチファイルの作成
        /// </summary>
        private static void CreateBatchFile_App(List<int> lstscanid)
        {

            if (lstscanid.Count == 0) return;

            string strBatch = string.Empty;
            int intcym = 0;// int.Parse(DateTime.Now.AddMonths(-3).ToString("yyyyMM").ToString());
            string strsql_app =
                $"select * from application where cym>={intcym} and scanid in (";
            foreach (int sid in lstscanid)
            {
                strsql_app += sid;
                strsql_app += ",";
            }
            strsql_app = strsql_app.Substring(0, strsql_app.Length - 1);
            strsql_app += ")";


            string strcopycommand = $"\\copy ({strsql_app}) TO %OUTPUTPATH%\\{strDBName}.sql";
            string strTruncatesql = "truncate table ocrimport_application";
            string strCopysql = $"\\copy ocrimport_application from %OUTPUTPATH%\\{strDBName}.sql";
            string strInsertNoConflict = $"insert into application select * from ocrimport_application on conflict do nothing";

            strBatch += $"@echo off\r\n";
            strBatch += $"set PGCLIENTENCODING={strEncode}\r\n";
            strBatch += $"set DBNAME={strDBName}\r\n";
            strBatch += $"set OUTPUTPATH={strOutputPath}\r\n";
            strBatch += $"set PSQLPATH={strPsqlPath}\r\n";
            strBatch += $"cd /d %PSQLPATH%\r\n";

            //レコードを選択しsqlファイルとして保存
            strBatch += $"echo %date%_%time%_%DBNAME%_download_application\r\n";
            strBatch += $"psql -h {strDBHost} -p {intDBPort} -U postgres -d %DBNAME% --command \"{strcopycommand}\";\r\n";
            
            //ocr機のテーブルをtruncate
            strBatch += $"echo %date%_%time%_%DBNAME% _truncate ocrimport_application\r\n";
            strBatch += $"psql {strLocalServer} --dbname %DBNAME% --command \"{strTruncatesql}\";\r\n";

            //ocr機にダウンロードしたsqlをインポート
            strBatch += $"echo %date%_%time%_%DBNAME%_copyto ocrimport_application\r\n";
            strBatch += $"psql {strLocalServer} --dbname %DBNAME% --command \"{strCopysql}\";\r\n";


            //一時テーブルからapplicationにコピー。重複は無視する
            strBatch += $"echo %date%_%time%_%DBNAME%_copy ocrimport_application to application \r\n";
            strBatch += $"psql {strLocalServer} --dbname %DBNAME% --command \"{strInsertNoConflict}\";\r\n";


          
#if DEBUG
            strBatch += $"pause\r\n";
#endif
            System.IO.StreamWriter sw = null;
            try
            {
                sw = new System.IO.StreamWriter(strOutputPath + $"\\{strDBName}_APP_{DateTime.Now.ToString("yyyyMMddHHmmss")}.bat");
                sw.WriteLine(strBatch);

            }
            catch (Exception ex)
            {
                ErrMessage(GetCurrentMethod().Name +"\r\n"+ ex.Message);
            }
            finally
            {
                sw.Close();
                RunBatch(strOutputPath + $"\\{strDBName}_APP_{DateTime.Now.ToString("yyyyMMddHHmmss")}.bat");
            }

        }
#endregion
        
        #region scanGroup取得バッチ
        /// <summary>
        /// awsからscangroupレコードをダウンロードしてくるバッチファイルの作成
        /// </summary>
        private static void CreateBatchFile_scangroup(List<int> lstscanid)
        {

            if (lstscanid.Count == 0) return;

            string strBatch = string.Empty;
            string strsql_scangroup =
                $"select * FROM scangroup WHERE scanid in (";
            foreach (int sid in lstscanid)
            {
                strsql_scangroup += sid;
                strsql_scangroup += ",";
            }
            strsql_scangroup = strsql_scangroup.Substring(0, strsql_scangroup.Length - 1);
            strsql_scangroup += ")";


            string strcopycommand = $"\\copy ({strsql_scangroup}) TO %OUTPUTPATH%\\%FILENAME%";

            string strTruncatesql = "truncate table ocrimport_scangroup";
            //string strTruncatesql = "truncate table scangroup";
            string strCopysql = $"\\copy ocrimport_scangroup from %OUTPUTPATH%\\%FILENAME%";
            string strInsertNoConflict = $"insert into scangroup select * from ocrimport_scangroup on conflict do nothing";


            strBatch += $"@echo off\r\n";
            strBatch += $"set PGCLIENTENCODING={strEncode}\r\n";
            strBatch += $"set DBNAME={strDBName}\r\n";
            strBatch += $"set FILENAME={strDBName}_scangroup.sql\r\n";
            strBatch += $"set OUTPUTPATH={strOutputPath}\r\n";
            strBatch += $"cd /d {strPsqlPath}\r\n";

            //レコードを選択しsqlファイルとして保存
            strBatch += $"echo %date%_%time%_%DBNAME%_download_scangroup\r\n";
            strBatch += $"psql -h {strDBHost} -p {intDBPort} -U postgres -d %DBNAME% --command \"{strcopycommand}\";\r\n";

            //2020/12/15　truncateすると、ocr途中のレコードも削除され、最初からになるのでやめる
            //ocr機のテーブルをtruncate 
            //strBatch += $"echo %date%_%time%_%DBNAME%_truncate_OCRDB\r\n";
            //strBatch += $"psql {strLocalServer} --dbname %DBNAME% --command \"{strTruncatesql}\";\r\n";


            strBatch += $"echo %date%_%time%_%DBNAME%_truncate ocrimport_scangroup\r\n";
            strBatch += $"psql {strLocalServer} --dbname %DBNAME% --command \"{strTruncatesql}\";\r\n";


            //ocr機にダウンロードしたsqlをインポート
            strBatch += $"echo %date%_%time%_%DBNAME%_copyto ocrimport_scangroup\r\n";
            strBatch += $"psql {strLocalServer} --dbname %DBNAME% --command \"{strCopysql}\";\r\n";


            //一時テーブルからscanにコピー。重複は無視する
            strBatch += $"echo %date%_%time%_%DBNAME%_copy ocrimport_scangroup to scangroup \r\n";
            strBatch += $"psql {strLocalServer} --dbname %DBNAME% --command \"{strInsertNoConflict}\";\r\n";


         
#if DEBUG
            strBatch += $"pause\r\n";
#endif
            System.IO.StreamWriter sw = null;
            try
            {
                sw = new System.IO.StreamWriter(strOutputPath + $"\\{strDBName}_SCANGROUP_{DateTime.Now.ToString("yyyyMMddHHmmss")}.bat");
                sw.WriteLine(strBatch);
                

            }
            catch (Exception ex)
            {
                ErrMessage(GetCurrentMethod().Name + "\r\n" + ex.Message);
            }
            finally
            {
                sw.Close();
                RunBatch(strOutputPath + $"\\{strDBName}_SCANGROUP_{DateTime.Now.ToString("yyyyMMddHHmmss")}.bat");
            }

        }
#endregion


        private static bool RunBatch(string strFileName)
        {

            System.Diagnostics.ProcessStartInfo si = new System.Diagnostics.ProcessStartInfo(strFileName);
            System.Diagnostics.Process p = new System.Diagnostics.Process();
            

//#if DEBUG
//            si.UseShellExecute = true;
//            si.CreateNoWindow = false;
//#else
//            si.UseShellExecute = false;
//            si.CreateNoWindow = true;
//#endif
            
            p.StartInfo = si;
            p.EnableRaisingEvents = true;

            if(!p.Start()) ErrMessage("バッチでエラー");
            p.WaitForExit();

     
            return true;
           // System.IO.File.Delete(strFileName);

        }
    }
}
